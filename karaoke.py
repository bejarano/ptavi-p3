#!/usr/bin/python3
# -*- coding: utf-8 -*-

import urllib.request
import json
import sys
import smallsmilhandler
from xml.sax import make_parser


class karaokelocal:

    def __init__(self, archivo):
        try:
            archivo = sys.argv[1]
            parser = make_parser()
            smil = smallsmilhandler.SmallSMILHandler()
            parser.setContentHandler(smil)
            parser.parse(open(archivo))
            self.all = smil.get_tags()

        except FlieNotFoundError:
            sys.exit('File not found')

    def __str__(self):
        r = ''
        for frase in self.all:
            for atributo in frase:
                if frase[atributo] != "":
                    r += atributo + "=" + "'" + frase[atributo] + "'" + '\t'
            r += '\n'
        return r

    # Crea y guarda el fichero json
    def to_json(self, smil):
        ficherojson = ''     # Inicializa el nombre del fichero#
        if ficherojson == '':
            # Cambia la extensión del fichero
            ficherojson = smil.replace('.smil', '.json')
        # Abrimos el fichero json en modo write
        with open(ficherojson, 'w') as f:
            # json.dump es una función que pasa un objeto a formato json
            json.dump(self.all, f, indent=4)

    def do_local(self):
        for frase in self.all:
            for atributo in frase:
                if atributo == 'src':
                    # Si el atributo empieza por http (es decir es una url)
                    # desde la ultima '/' hasta el final es 'direccion'
                    if frase[atributo].startswith('http://'):
                        direccion = frase[atributo].split('/')[-1]
                # Descargar el archivo
                        urllib.request.urlretrieve(frase[atributo])
                        print("Descargando... (͠≖ ͜ʖ͠≖)👌")

                        frase[atributo] = direccion


if __name__ == "__main__":
    # Programa principal

    try:
        archivo = sys.argv[1]
        karaoke = karaokelocal(archivo)
    except:
        sys.exit('usage error: python3 karaoke.py file.smil')
    print(karaoke)
    karaoke.to_json(archivo)
    karaoke.do_local()
    karaoke.to_json('local.smil')
    print(karaoke)
